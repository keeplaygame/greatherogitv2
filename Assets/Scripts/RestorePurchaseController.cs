using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Prime31;


public class RestorePurchaseController : MonoBehaviourGUI
{
	// Identificador do produto
	private string str_productIdentifier;

	// Mostra quantos produtos foram restaurados
	private int i_restoredProducts;

	// Indica se está restaurando uma compra
	private bool b_isRestoringPurchases;

#if UNITY_IPHONE
	private List<StoreKitProduct> _products;

	void OnEnable()
	{
		// Escuta os seguintes eventos
		StoreKitManager.productListReceivedEvent += productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent += productListRequestFailedEvent;
		StoreKitManager.purchaseSuccessfulEvent += purchaseSuccessfulEvent;
		StoreKitManager.restoreTransactionsFailedEvent += restoreTransactionsFailedEvent;
		StoreKitManager.restoreTransactionsFinishedEvent += restoreTransactionsFinishedEvent;
	}
	
	void OnDisable()
	{
		// Para de escutar os eventos
		StoreKitManager.productListReceivedEvent -= productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent -= productListRequestFailedEvent;
		StoreKitManager.purchaseSuccessfulEvent -= purchaseSuccessfulEvent;
		StoreKitManager.restoreTransactionsFailedEvent -= restoreTransactionsFailedEvent;
		StoreKitManager.restoreTransactionsFinishedEvent -= restoreTransactionsFinishedEvent;
	}

	void Start()
	{
		// Seta que inicialmente 0 produtos foram restaurados
		i_restoredProducts = 0;

		// Seta que não está restaurando uma compra
		b_isRestoringPurchases = false;

		// Verifica se os produtos ainda não foram requisitados
		if (PlayerPrefs.GetInt("iIsProductsRequested", 0) == 0)
		{
			// Requisita os produtos da loja
			RequestProducts();
		}
	}

	/// <summary>
	/// Restores the purchases.
	/// </summary>
	public void RestorePurchases ()
	{
		// Mostra o indicador de atividade
		LoadingController.ShowActivity();

		// Verifica se os produtos foram requisitados
		if (PlayerPrefs.GetInt("iIsProductsRequested", 0) == 1)
		{
			// Seta que não precisa restaurar as compras novamente
			b_isRestoringPurchases = false;

			// Restaura as compras já efetuadas
			StoreKitBinding.restoreCompletedTransactions();
		}

		// Os produtos não foram requisitados
		else
		{
			// Seta que está restaurando as compras
			b_isRestoringPurchases = true;

			// Requisita os produtos da loja
			RequestProducts();
		}
	}

	/// <summary>
	/// Products the list received event.
	/// </summary>
	/// <param name="productList">Product list.</param>
	void productListReceivedEvent( List<StoreKitProduct> productList )
	{
		Debug.Log( "productListReceivedEvent. total products received: " + productList.Count );
		
		// print the products to the console
		foreach( StoreKitProduct product in productList )
			Debug.Log(product.ToString() + "\n" );

		// Seta que os produtos foram requisitados
		PlayerPrefs.SetInt("iIsProductsRequested", 1);

		// Verifica se está restaurando as compras
		if (b_isRestoringPurchases)
		{
			// Restaura as compras
			RestorePurchases();
		}
	}

	/// <summary>
	/// Products the list request failed event.
	/// </summary>
	/// <param name="error">Error.</param>
	void productListRequestFailedEvent( string error )
	{
		Debug.Log( "productListRequestFailedEvent: " + error );

		// Esconde o indicador de atividade
		LoadingController.HideActivity();

		// Verifica se está restaurando as compras
		if (b_isRestoringPurchases)
		{
			// Verifica se o idioma é português
			if (PlayerPrefs.GetInt("LanguageSelected") == 1)
			{
				// Mensagem de restauração falhada
				EtceteraBinding.showAlertWithTitleMessageAndButton("Sem conexão de internet. Tente novamente mais tarde.", "", "OK");
			}

			// O idioma é o inglês
			else
			{
				// Mensagem de restauração falhada
				EtceteraBinding.showAlertWithTitleMessageAndButton("No internet access. Try again later.", "", "OK");
			}
		}

		// Seta que não precisa restaurar as compras
		b_isRestoringPurchases = false;
	}

	/// <summary>
	/// Purchases the successful event.
	/// </summary>
	/// <param name="transaction">Transaction.</param>
	void purchaseSuccessfulEvent( StoreKitTransaction transaction )
	{
		Debug.Log( "purchaseSuccessfulEvent: " + transaction );

		// Adiciona um produto restaurado
		i_restoredProducts++;

		// Verifica qual o produto comprado
		switch (transaction.productIdentifier)
		{
			// Caso sejam as fases do Davi e Golias
		case "DavidAndGoliathLevels":

			// Seta que as fases do episodio Davi e Golias estão destravadas
			PlayerPrefs.SetInt("isEpisode" + "DavidAndGoliath" + "LevelsUnLocked", 1);

			break;

			// Caso sejam as fases da arca de noé
		case "NoahsArkLevels":

			// Seta que as fases do episodio Arca de Noé estão destravadas
			PlayerPrefs.SetInt("isEpisode" + "NoahsArk" + "LevelsUnLocked", 1);

			break;
		}
	}

	/// <summary>
	/// Restores the transactions failed event.
	/// </summary>
	/// <param name="error">Error.</param>
	void restoreTransactionsFailedEvent( string error )
	{
		Debug.Log( "restoreTransactionsFailedEvent: " + error );

		// Esconde o indicador de atividade
		LoadingController.HideActivity();

		// Verifica se o idioma é português
		if (PlayerPrefs.GetInt("LanguageSelected") == 1)
		{
			// Mensagem de restauração falhada
			EtceteraBinding.showAlertWithTitleMessageAndButton("Sem conexão de internet. Tente novamente mais tarde.", "", "OK");
		}
		
		// O idioma é o inglês
		else
		{
			// Mensagem de restauração falhada
			EtceteraBinding.showAlertWithTitleMessageAndButton("No internet access. Try again later.", "", "OK");
		}
	}
	
	/// <summary>
	/// Restores the transactions finished event.
	/// </summary>
	void restoreTransactionsFinishedEvent()
	{
		Debug.Log( "restoreTransactionsFinished" );

		// Esconde o indicador de atividade
		LoadingController.HideActivity();

		// Inicia a corotina de restauração finalizada 
		StartCoroutine("RestoreFinished");
	}

	/// <summary>
	/// Restores the finished.
	/// </summary>
	/// <returns>The finished.</returns>
	IEnumerator RestoreFinished ()
	{
		// Aguarda para dar tempo do indicador de produtos restaurados incrementar
		yield return new WaitForSeconds (0.00001f);

		// Verifica se o idioma é português
		if (PlayerPrefs.GetInt("LanguageSelected") == 1)
		{
			// Mensagem de restauração de compras finalizadas
			EtceteraBinding.showAlertWithTitleMessageAndButton("Restauração concluída.", "", "OK");
		}
		
		// O idioma é o inglês
		else
		{
			// Mensagem de restauração de compras finalizadas
			EtceteraBinding.showAlertWithTitleMessageAndButton("Restoration complete.", "", "OK");
		}

		// Reseta o valor de produtos restaurados
		i_restoredProducts = 0;
	}

	/// <summary>
	/// Requests the products.
	/// </summary>
	private void RequestProducts ()
	{
		// you cannot make any purchases until you have retrieved the products from the server with the requestProductData method
		// we will store the products locally so that we will know what is purchaseable and when we can purchase the products.
//		StoreKitManager.productListReceivedEvent += allProducts =>
//		{
//			Debug.Log( "received total products: " + allProducts.Count );
//			_products = allProducts;
//
//			// Verifica se está restaurando as compras
//			if (b_isRestoringPurchases)
//			{
//				// Restaura as compras
//				RestorePurchases();
//			}
//		};
		
		// array of product ID's from iTunesConnect. MUST match exactly what you have there!
		//var productIdentifiers = new string[] { "DavidAndGoliathLevels", "NoahsArkLevels" };
		var productIdentifiers = new string[] { "DavidAndGoliathLevels", "NoahsArkLevels" };
		StoreKitBinding.requestProductData( productIdentifiers );
	}

#endif
}
