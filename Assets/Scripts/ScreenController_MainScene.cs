﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31;

public class ScreenController_MainScene : MonoBehaviour 
{
	// Enumerador com todas as telas do jogo
	enum Screens
	{
		Splash01,
		Splash02,
		LanguageSelection,
		Home,
		EpisodesSelection,
		Creditos,
		Options,
		Stamps,
	}

	private Screens enum_actualScreen; // Indica a tela atual do jogo
	private Screens enum_lastScreen; // Indica a tela anterior

	// Decorator Drawer.
	[Header ("Cameras")]

	//
	// CAMERAS Inicio.
	//
	public Camera cam_splash01; // Camera da tela de splash 01
	public Camera cam_splash02; // Camera da tela de splash 02
	public Camera cam_languageSelection; // Camera de selecao de idioma
	public Camera cam_home; // Camera da tela de home
	public Camera cam_episodesSelection; // Camera da tela de home
	public Camera cam_creditos; // Camera da tela de credito
	public Camera cam_options; // Camera da tela de OPÇÕES.
	public Camera cam_stamps; // Camera da tela dos SELOS.
	//
	// CAMERAS Fim.
	//

	// Indica o idioma selecionado
	private int i_languageSelected;

	// Indica se estah mudando de camera.
	private bool b_isChangingScreen; 

	// Indica se os botoes da tela estao ativos.
	public bool b_isScreenButtonsActive;

	// Indica se a Splash foi vista.
	private int i_isSplashShowed;

	// Indica se deve mostrar a galeria de selos
	private int i_isToShowStampGallery;

	// Decorator Drawer.
	[Header ("Controllers")]

	[SerializeField]
	// Recebe e guarda o stampGalleryController
	private StampGalleryController stampGalleryController;

	// Recebe e guarda EpisodesSelection Script.
	public EpisodesSelection episodesSelection;

	// Recebe e guarda o GameObject que tem o AudioController Script.
	public AudioController audioController;

	[SerializeField]
	// Recebe e guarda o HomeController.
	private HomeController homeController;

	//Needs to be static.
	private static bool spawned = false;

	void Awake ()
	{
		// Para o indicador de atividade
		LoadingController.HideActivity();
	}

	// Use this for initialization
	void Start () 
	{
		// Inicia na tela de splash 01
		enum_actualScreen = Screens.Splash01; 

		// Seta que nao estah mudando de tela.
		b_isChangingScreen = false;

		// Seta que os botoes da tela estao ativos.
		b_isScreenButtonsActive = true;

		// Seta que a principio a splash nao foi mostrada
		i_isSplashShowed = PlayerPrefs.GetInt("iIsSplashShowed");

		// Seta que nenhum idioma foi selecionado a principio
		i_languageSelected = 0;

		// Inicia a corotina do tempo das telas de splash
		StartCoroutine(SplashScreenTimer());
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Verifica se tem mudança de tela
		CheckChangeScreen();
	}

	/// <summary>
	/// Checks the change screen.
	/// </summary>
	void CheckChangeScreen ()
	{
		// Verifica se estah mudando de tela
		if (b_isChangingScreen)
		{
			// Desabilita todas as cameras
			cam_splash01.gameObject.SetActive(false);
			cam_splash02.gameObject.SetActive(false);
			cam_languageSelection.gameObject.SetActive(false);
			cam_home.gameObject.SetActive(false);
			cam_episodesSelection.gameObject.SetActive(false);
			cam_creditos.gameObject.SetActive(false);
			cam_options.gameObject.SetActive(false);
			cam_stamps.gameObject.SetActive(false);

			// Verifica qual eh a tela atual
			switch (enum_actualScreen)
			{
				// Caso seja a tela de splash 01
			case Screens.Splash01:

				// Habilita a camera de splash 01
				cam_splash01.gameObject.SetActive(true);

				break;

				// Caso seja a tela de splash 02
			case Screens.Splash02:

				// Habilita a camera de splash 02
				cam_splash02.gameObject.SetActive(true);

				break;

			case Screens.LanguageSelection:

				// Habilita a camera de selecao de idioma
				cam_languageSelection.gameObject.SetActive(true);

				break;

				// Caso seja a tela de home
			case Screens.Home:

				// Habilita a camera da tela de home
				cam_home.gameObject.SetActive(true);

				// Executa funçao para definir qual botao mostrar de acordo com o idioma selecionado.
				homeController.SetStampsBtn();

				break;

				// Caso seja a tela de SELEÇÃO DE EPISÓDIOS.
			case Screens.EpisodesSelection:
				
				// Habilita a camera da tela de SELEÇÃO DE EPISÓDIOS.
				cam_episodesSelection.gameObject.SetActive(true);

				break;

				// Caso seja a tela de creditos
			case Screens.Creditos:

				// Habilita a tela de creditos
				cam_creditos.gameObject.SetActive(true);

				break;

				// Caso seja a tela de OPÇÕES.
			case Screens.Options:
				
				// Habilita a tela de OPÇÕES.
				cam_options.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela dos SELOS.
			case Screens.Stamps:
				
				// Habilita a tela de SELOS.
				cam_stamps.gameObject.SetActive(true);

				// Reinica a galeria de selos
				stampGalleryController.RestartStampGallery();
				
				break;

			default:
				
				break;
			}

			// EXECUTA função para o LANGUAGE.CS atualizar o idioma da tela. 
			Language.SwitchLanguage(Language.CurrentLanguage());

			// Seta que mudou de tela
			b_isChangingScreen = false; 

			// Seta que os botoes estah ativos
			b_isScreenButtonsActive = true;

			// Usar aqui?? Mas não é assincrono?
			Resources.UnloadUnusedAssets();
		}
	}

	/// <summary>
	/// Sets the screen buttons not active.
	/// </summary>
	public void SetScreenButtonsNotActive ()
	{
		// Seta que os botoes da tela nao estao ativos
		b_isScreenButtonsActive = false;
	}

	/// <summary>
	/// Splash screen timer.
	/// </summary>
	/// <returns>The screen timer.</returns>
	IEnumerator SplashScreenTimer ()
	{
		// Verifica se ainda nao mostrou a splash
		if (i_isSplashShowed == 0)
		{
			// Seta o tempo da tela de splash
			float fSplashTime = 1.5f;

			// Aguarda o tempo para mudar de tela
			yield return new WaitForSeconds (fSplashTime);

			// Guarda a tela anterior
			enum_lastScreen = enum_actualScreen; 
			
			// Carrega a tela de splash 02
			enum_actualScreen = Screens.Splash02;

			// Seta que está mudando de tela
			b_isChangingScreen = true;

			// Aguarda o tempo para mudar de tela
			yield return new WaitForSeconds (fSplashTime);

			// Grava que já mostrou a splash
			PlayerPrefs.SetInt("iIsSplashShowed", 1);
			
			// Seta que a splash foi mostrada
			i_isSplashShowed = 1;

			// Carrega o idioma selecionado
			LoadLanguage();
		}

		// Já mostrou a splash
		else
		{
			// Guarda a tela anterior
			enum_lastScreen = enum_actualScreen; 

			// Verifica se não é para mostrar a galeria de selos
			if (PlayerPrefs.GetInt("iIsToShowStampGallery", 0) == 0)
			{
				// Carrega a tela de home
				enum_actualScreen = Screens.EpisodesSelection;
			}

			// É para mostrar a galeria de selos
			else
			{
				// Seta que ja mostrou a galeria de selos
				PlayerPrefs.SetInt("iIsToShowStampGallery", 0);

				// Carrega a tela de home
				enum_actualScreen = Screens.Stamps;
			}
		}

		// Seta que estah mudando de tela
		b_isChangingScreen = true;
	}

	/// <summary>
	/// Loads the language.
	/// </summary>
	private void LoadLanguage ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen; 
		
		// Carrega a tela de home
		enum_actualScreen = Screens.Home;

		// Verifica se foi escolhido o idioma Portuguese.
		if (PlayerPrefs.GetInt("LanguageSelected") == 1)
		{
			// Seta que foi selecionado o idioma Portuguese.
			i_languageSelected = 1;

			// Seta o idioma para português
			Language.SwitchLanguage(LanguageCode.PT);
		}
		
		// Verifica se foi escolhido o idioma English.
		else if (PlayerPrefs.GetInt("LanguageSelected") == 2) 
		{
			// Seta que foi selecionado o idioma English.
			i_languageSelected = 2;

			/// Seta o idioma para inglês
			Language.SwitchLanguage(LanguageCode.EN);
		}

		// Nenhum idioma foi selecionado
		else
		{
			// Carrega a tela de selecionar idioma
			enum_actualScreen = Screens.LanguageSelection;
		}
	}

	public int GetLanguage ()
	{
		return i_languageSelected;	
	}

	#region case void SetScreen
	/// <summary>
	/// Sets the screen.
	/// </summary>
	public void SetAction (string strActionName)
	{
		// Verifica se os botoes da tela estah ativos.
		if (b_isScreenButtonsActive)
		{
			// Verifica qual eh o Nome da Acao.
			switch (strActionName)
			{
				// Caso seja a tela de HOME.
			case "Home":
				
				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual eh de HOME.
				enum_actualScreen = Screens.Home;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;

				break;

				// Caso seja a tela de SELEÇÃO DE EPISÓDIOS.
			case "EpisodesSelection":
				
				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual eh de SELEÇÃO DE EPISÓDIOS.
				enum_actualScreen = Screens.EpisodesSelection;
				
				// Seta que estah mudando de tela
				b_isChangingScreen = true;

				break;

				// Caso seja a tela de Creditos.
			case "Creditos":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual eh de Creditos.
				enum_actualScreen = Screens.Creditos;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;

				break;

				// Caso seja a tela de OPÇÕES.
			case "Options":
				
				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual é de OPÇÕES.
				enum_actualScreen = Screens.Options;
				
				// Seta que estah mudando de tela
				b_isChangingScreen = true;

				break;

				// Caso seja a tela dos SELOS.
			case "Stamps":
				
				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Seta que a camera atual é de SELOS.
				enum_actualScreen = Screens.Stamps;
				
				// Seta que estah mudando de tela
				b_isChangingScreen = true;

				break;

			default:
				
				break;
			}
		}
	}
	#endregion

	/// <summary>
	/// Raises the application quit event.
	/// </summary>
	void OnApplicationQuit ()
	{
		// DELETA a Key da Splash.
		PlayerPrefs.DeleteKey ("iIsSplashShowed");
	}
}