﻿using UnityEngine;
using System.Collections;

public class DragControllerModulo7 : MonoBehaviour 
{
	// Indica o numero do item
	public int i_itemNumber;

	// Recebe o modulo7 controller
	public Modulo7Controller modulo7Controller;

	// Guarda o nome do áudio que toca quando acerta.
	private string str_audioRightResourceName;
	
	[SerializeField]
	[Tooltip ("É o caminho onde está o áudio.")]
	// Nome do caminho do áudio que toca quando acerta a posição.
	private string str_audioRightPath;

	[Tooltip ("É o para colocar o Shadow aqui.")]
	// Objeto Shadow que serah ativado quando colocado no lugar certo
	public GameObject go_shadowAtRightPlace;
	
	[SerializeField]
	// Objeto que serah ativado quando colocado no lugar certo
	private GameObject go_atRightPlace;
	
	[SerializeField]
	// RECEBE partícula a ser instanciada quando o objeto é acertado.
	private GameObject go_particle;

	// Som de encaixe ao acertar a posicao
	public AudioClip audio_right;
	
	// Som de erro ao errar a posicao
	public AudioClip audio_wrong;

	// Som quando volta o item ao lugar
	public AudioClip audio_back;

	// Guarda a posicao do objeto na posicao correta
	private Vector3 vec3_atRightPlaceTransformPosition;
	
	// Guarda a posicao anterior do mouse
	private Vector3 vec3_previousMousePosition;
	
	// Guarda a escala inicial
	private Vector3 vec3_initialLocalScale;
	
	// Valor que multiplicara para o aumento da escala
	private float f_scaleMultiplier;

	// Indica se estah sendo arrastado
	private bool b_isDragging;

	// Indica se estah no lugar certo
	private bool b_isAtRightPlace;
	
	// Guarda a textura normal
	public Texture texture_normal;

	void Awake ()
	{
		// PEGA o nome do GameObject e adiciona _right para pegar o som correto. Dessa maneira fica automatizado.
		str_audioRightResourceName = this.gameObject.name + "_right";
		
		// Carrega o áudio que toca quando acerta a posição do objeto
		audio_right = Resources.Load(str_audioRightPath + str_audioRightResourceName) as AudioClip;
	}
	
	// Use this for initialization
	void Start () 
	{
		// Seta que o botao nao comeca sendo arrastado
		b_isDragging = false;

		// Seta que nao estah no lugar certo no comeco
		b_isAtRightPlace = false;

		// Guarda a posicao inicial da posicao correta
		vec3_atRightPlaceTransformPosition = go_shadowAtRightPlace.transform.position;

		// SETA que a escala inicial eh igual a ZERO.
		vec3_initialLocalScale = Vector3.zero;
		
		// Aumenta 20% do botao
		f_scaleMultiplier = 1.2f;

		// SETA que a textura é igual a textura que está no componente GuiTexture.
		texture_normal = this.guiTexture.texture;
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Verifica e executa a posicao quando o botao estiver sendo arrastado
		DraggingPosition();

		// Verifica se reinicia a tela
		CheckRestartScreen();
	}
	
	void OnMouseExit()
	{
		// Verifica se nao estah sendo arrastado e se nao estah no lugar certo
		if (!b_isDragging && !b_isAtRightPlace)
		{
			// Volta a textura normal do botao
			guiTexture.texture = texture_normal;
		}
	}
	
	void OnMouseDown()
	{
		// Verifica se nao estah no lugar certo
		if (!b_isAtRightPlace)
		{
			// Verifica se ainda nao guardou a escala inicial.
			if(vec3_initialLocalScale == Vector3.zero)
			{
				// Guarda a escala inicial
				vec3_initialLocalScale = this.transform.localScale;
			}

			// Seta a textura hover do botao
			guiTexture.texture = texture_normal;
			
			// Começa a ser arrastado
			Drag();
		}
	}
	
	void OnMouseUp()
	{
		// Verifica se nao estah no lugar certo
		if (!b_isAtRightPlace)
		{
			// Volta a textura normal do botao
			guiTexture.texture = texture_normal;
			
			// Verifica se estah sendo arrastado
			if (b_isDragging)
			{
				// Seta que nao estah mais sendo arrastado
				b_isDragging = false;
				
				// Joga o botao para a camada original
				this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 6.27f);
				
				// Volta a escala original
				this.transform.localScale = vec3_initialLocalScale;
				
				// Verifica a posicao que foi solto o objeto
				CheckReleasePosition();
			}
		}
	}
	
	/// <summary>
	/// Drag this instance.
	/// </summary>
	private void Drag ()
	{
		// Volta a textura normal do botao
		guiTexture.texture = texture_normal;
				
		// Seta que estah sendo arrastado
		b_isDragging = true;

		// Aumenta a escala
		this.transform.localScale *= f_scaleMultiplier;
		
		// Joga o botao para frente
		this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 10.0f);
		
		// Guarda a posicao do mouse no comeco do drag
		vec3_previousMousePosition = Input.mousePosition;
	}

	/// <summary>
	/// Checks the release position.
	/// </summary>
	private void CheckReleasePosition ()
	{
		// Verifica qual eh o tipo do item
		switch (i_itemNumber)
		{
			
			// Caso seja item 0
		case 0:

			// Verifica se estah na posicao correta
			if (Input.mousePosition.x > (Screen.width * vec3_atRightPlaceTransformPosition.x) - (Screen.width * 0.15f) &&
			    Input.mousePosition.x < (Screen.width * vec3_atRightPlaceTransformPosition.x) + (Screen.width * 0.15f) &&
			    Input.mousePosition.y > (Screen.height * vec3_atRightPlaceTransformPosition.y) - (Screen.height * 0.2f) && 
			    Input.mousePosition.y < (Screen.height * vec3_atRightPlaceTransformPosition.y) + (Screen.height * 0.2f))
			{
				// Ativa o objeto da posicao correta
				go_atRightPlace.SetActive(true);
				
				#region Usado para a Partícula.
				// DEFINE posição da partícula a partir da posição do GameObject do tipo GUI.
				// Isso porque os objetos do tipo GUI possuem sistema de posição diferente dos outros objetos.
				Vector3 vec3_guiPositionForParticle = Camera.main.ViewportToWorldPoint(new Vector3(go_shadowAtRightPlace.transform.position.x,
				                                                                                   go_shadowAtRightPlace.transform.position.y,
				                                                                                   8.99f));
				
				// INSTANCIA a partícula no lugar do objeto GUI que foi colocado no lugar correto.
				Instantiate(go_particle, vec3_guiPositionForParticle, go_atRightPlace.transform.rotation);
				#endregion

				// Seta para ficar sem textura
				guiTexture.texture = null;
				
				// Seta que estah no lugar certo
				b_isAtRightPlace = true;
				
				// Toca o som de acerto
				audio.PlayOneShot(audio_right, 1.0f);
				
				// Seta que o item estah na posicao correta
				modulo7Controller.SetItemAtRightPlace(i_itemNumber, true);
			}
			
			// Solta na posicao original
			else if (Input.mousePosition.x > Screen.width * 0.81f)
			{
				// Toca o som de solto
				audio.PlayOneShot(audio_back, 1.0f);

				// Volta a posicao original
				this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);
			}
			
			// Posicao errada
			else
			{
				// Toca o som de erro
				audio.PlayOneShot(audio_wrong, 1.0f);

				// Volta a posicao original
				this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);
			}
			
			break;

			// Caso seja item 1
		case 1:
			
			// Verifica se estah na posicao correta
			if (Input.mousePosition.x > (Screen.width * vec3_atRightPlaceTransformPosition.x) - (Screen.width * 0.2f) &&
			    Input.mousePosition.x < (Screen.width * vec3_atRightPlaceTransformPosition.x) + (Screen.width * 0.2f) &&
			    Input.mousePosition.y > (Screen.height * vec3_atRightPlaceTransformPosition.y) - (Screen.height * 0.2f) && 
			    Input.mousePosition.y < (Screen.height * vec3_atRightPlaceTransformPosition.y) + (Screen.height * 0.2f))
			{
				// Ativa o objeto da posicao correta
				go_atRightPlace.SetActive(true);
				
				#region Usado para a Partícula.
				// DEFINE posição da partícula a partir da posição do GameObject do tipo GUI.
				// Isso porque os objetos do tipo GUI possuem sistema de posição diferente dos outros objetos.
				Vector3 vec3_guiPositionForParticle = Camera.main.ViewportToWorldPoint(new Vector3(go_shadowAtRightPlace.transform.position.x,
				                                                                                   go_shadowAtRightPlace.transform.position.y,
				                                                                                   8.99f));
				
				// INSTANCIA a partícula no lugar do objeto GUI que foi colocado no lugar correto.
				Instantiate(go_particle, vec3_guiPositionForParticle, go_atRightPlace.transform.rotation);
				#endregion

				// Seta para ficar sem textura
				guiTexture.texture = null;
				
				// Seta que estah no lugar certo
				b_isAtRightPlace = true;
				
				// Toca o som de acerto
				audio.PlayOneShot(audio_right, 1.0f);
				
				// Seta que o item estah na posicao correta
				modulo7Controller.SetItemAtRightPlace(i_itemNumber, true);
			}
			
			// Solta na posicao original
			else if (Input.mousePosition.x > Screen.width * 0.81f)
			{
				// Toca o som de solto
				audio.PlayOneShot(audio_back, 1.0f);
				
				// Volta a posicao original
				this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);
			}
			
			// Posicao errada
			else
			{
				// Toca o som de erro
				audio.PlayOneShot(audio_wrong, 1.0f);
				
				// Volta a posicao original
				this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);
			}
			
			break;

			// Caso seja item 2
		case 2:
			
			// Verifica se estah na posicao correta
			if (Input.mousePosition.x > (Screen.width * vec3_atRightPlaceTransformPosition.x) - (Screen.width * 0.2f) &&
			    Input.mousePosition.x < (Screen.width * vec3_atRightPlaceTransformPosition.x) + (Screen.width * 0.2f) &&
			    Input.mousePosition.y > (Screen.height * vec3_atRightPlaceTransformPosition.y) - (Screen.height * 0.2f) && 
			    Input.mousePosition.y < (Screen.height * vec3_atRightPlaceTransformPosition.y) + (Screen.height * 0.2f))
			{
				// Ativa o objeto da posicao correta
				go_atRightPlace.SetActive(true);
				
				#region Usado para a Partícula.
				// DEFINE posição da partícula a partir da posição do GameObject do tipo GUI.
				// Isso porque os objetos do tipo GUI possuem sistema de posição diferente dos outros objetos.
				Vector3 vec3_guiPositionForParticle = Camera.main.ViewportToWorldPoint(new Vector3(go_shadowAtRightPlace.transform.position.x,
				                                                                                   go_shadowAtRightPlace.transform.position.y,
				                                                                                   8.99f));
				
				// INSTANCIA a partícula no lugar do objeto GUI que foi colocado no lugar correto.
				Instantiate(go_particle, vec3_guiPositionForParticle, go_atRightPlace.transform.rotation);
				#endregion

				// Seta para ficar sem textura
				guiTexture.texture = null;
				
				// Seta que estah no lugar certo
				b_isAtRightPlace = true;
				
				// Toca o som de acerto
				audio.PlayOneShot(audio_right, 1.0f);
				
				// Seta que o item estah na posicao correta
				modulo7Controller.SetItemAtRightPlace(i_itemNumber, true);
			}
			
			// Solta na posicao original
			else if (Input.mousePosition.x > Screen.width * 0.81f)
			{
				// Toca o som de solto
				audio.PlayOneShot(audio_back, 1.0f);
				
				// Volta a posicao original
				this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);
			}
			
			// Posicao errada
			else
			{
				// Toca o som de erro
				audio.PlayOneShot(audio_wrong, 1.0f);
				
				// Volta a posicao original
				this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);
			}
			
			break;

			// Caso seja item 3
		case 3:
			
			// Verifica se estah na posicao correta
			if (Input.mousePosition.x > (Screen.width * vec3_atRightPlaceTransformPosition.x) - (Screen.width * 0.06f) &&
			    Input.mousePosition.x < (Screen.width * vec3_atRightPlaceTransformPosition.x) + (Screen.width * 0.06f) &&
			    Input.mousePosition.y > (Screen.height * vec3_atRightPlaceTransformPosition.y) - (Screen.height * 0.1f) && 
			    Input.mousePosition.y < (Screen.height * vec3_atRightPlaceTransformPosition.y) + (Screen.height * 0.1f))
			{
				// Ativa o objeto da posicao correta
				go_atRightPlace.SetActive(true);
				
				#region Usado para a Partícula.
				// DEFINE posição da partícula a partir da posição do GameObject do tipo GUI.
				// Isso porque os objetos do tipo GUI possuem sistema de posição diferente dos outros objetos.
				Vector3 vec3_guiPositionForParticle = Camera.main.ViewportToWorldPoint(new Vector3(go_shadowAtRightPlace.transform.position.x,
				                                                                                   go_shadowAtRightPlace.transform.position.y,
				                                                                                   8.99f));
				
				// INSTANCIA a partícula no lugar do objeto GUI que foi colocado no lugar correto.
				Instantiate(go_particle, vec3_guiPositionForParticle, go_atRightPlace.transform.rotation);
				#endregion

				// Seta para ficar sem textura
				guiTexture.texture = null;
				
				// Seta que estah no lugar certo
				b_isAtRightPlace = true;
				
				// Toca o som de acerto
				audio.PlayOneShot(audio_right, 1.0f);
				
				// Seta que o item estah na posicao correta
				modulo7Controller.SetItemAtRightPlace(i_itemNumber, true);
			}
			
			// Solta na posicao original
			else if (Input.mousePosition.x > Screen.width * 0.81f)
			{
				// Toca o som de solto
				audio.PlayOneShot(audio_back, 1.0f);
				
				// Volta a posicao original
				this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);
			}
			
			// Posicao errada
			else
			{
				// Toca o som de erro
				audio.PlayOneShot(audio_wrong, 1.0f);
				
				// Volta a posicao original
				this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);
			}
			
			break;
		}
	}
	
	/// <summary>
	/// Draggings the position.
	/// </summary>
	private void DraggingPosition ()
	{
		// Verifica se estah sendo arrastado
		if (b_isDragging)
		{
			// Atualiza a posicao do botao
			guiTexture.pixelInset = new Rect(guiTexture.pixelInset.x + (Input.mousePosition.x - vec3_previousMousePosition.x), 
			                                 guiTexture.pixelInset.y + (Input.mousePosition.y - vec3_previousMousePosition.y), 
			                                 guiTexture.pixelInset.width, guiTexture.pixelInset.height);

			// Atualiza a posicao do mouse
			vec3_previousMousePosition = Input.mousePosition;
		}
	}
	
	/// <summary>
	/// Determines whether this instance is dragging.
	/// </summary>
	/// <returns><c>true</c> if this instance is dragging; otherwise, <c>false</c>.</returns>
	public bool IsDragging ()
	{
		// Retorna se a ferramenta estah sendo carregada
		return b_isDragging;
	}

	/// <summary>
	/// Checks the restart screen.
	/// </summary>
	private void CheckRestartScreen ()
	{
		// Verifica se estah reiniciando o modulo
		if (modulo7Controller.IsRestartingModulo())
		{
			// Seta que nao estah no lugar certo
			b_isAtRightPlace = false;
			
			// Desativa o objeto da posicao correta
			go_atRightPlace.SetActive(false);

			// Volta o objeto a posicao inicial
			this.guiTexture.pixelInset = new Rect(0, 0, 0, 0);

			// Volta a textura normal do objeto
			this.guiTexture.texture = texture_normal;
			
			// Seta que o item nao estah na posicao correta
			modulo7Controller.SetItemAtRightPlace(i_itemNumber, false);
		}
	}
}
