﻿using UnityEngine;
using System.Collections;

public class ButtonControllerMainScene : MonoBehaviour 
{
	// Recebe o Screen Controller
	public ScreenController_MainScene screenController_MainScene;

	// Guarda o nome da acao.
	public string str_actionName;

	// Indica se o botao estah ativo
	private bool b_isActive;

	// Guarda a textura normal
	public Texture2D texture_normal;

	// Guarda a textura de hover
	public Texture2D texture_hover;

	void Start ()
	{
		// Seta que o bota nao comeca ativo
		b_isActive = false;

		// SETA a textura do Guitextura para ser a textura normal.
		guiTexture.texture = texture_normal;
	}


	void OnMouseExit()
	{
		// Volta a textura normal do botao
		guiTexture.texture = texture_normal;

		// Seta que o botao nao estah ativo
		b_isActive = false;
	}

	void OnMouseDown()
	{
		// Seta a textura hover do botao
		guiTexture.texture = texture_hover;

		// Seta que o botao estah ativo
		b_isActive = true;
	}

	void OnMouseUp()
	{
		// Verifica se o botao estah ativo
		if (b_isActive)
		{
			// Volta a textura normal do botao
			guiTexture.texture = texture_normal;
			
			// Envia para o screen controller a acao.
			screenController_MainScene.SetAction(str_actionName);
		}
	}
}
