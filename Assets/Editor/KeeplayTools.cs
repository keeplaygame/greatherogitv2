﻿using UnityEngine;
using UnityEditor;

public class KeeplayTools
{
	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay Tools/Clear Player Prefs.
	[MenuItem("Keeplay Tools/Clear PlayerPrefs")]

	// Cria uma função static para o botão.
	private static void DeleteAllPlayerPrefs()
	{
		// Deleta os PlayerPrefs.
		PlayerPrefs.DeleteAll();

		// Imprime mensagem no Console.
		Debug.Log ("PlayerPref DELETADOS");
	}
}