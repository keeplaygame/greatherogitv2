﻿using UnityEngine;
using System.Collections;

// *****
// Criado por Fcosta Keeplay.


public class FeedbacksController : MonoBehaviour 
{
	// Guarda os gameObject das Animacoes da telas de feedback.
	public GameObject go_animations1;
	public GameObject go_animations2;
	public GameObject go_animations3;
	public GameObject go_animations4;
	public GameObject go_animations5;
	public GameObject go_animations6;
	public GameObject go_animations7;
	public GameObject go_animations8;
	public GameObject go_animations9;
	public GameObject go_animations10;

	// Use this for initialization
	void Start () 
	{
		// Seta que as animacoes devem comecar DESATIVADAS.
		go_animations1.gameObject.SetActive (false);
		go_animations2.gameObject.SetActive (false);
		go_animations3.gameObject.SetActive (false);
		go_animations4.gameObject.SetActive (false);
		go_animations5.gameObject.SetActive (false);
		go_animations6.gameObject.SetActive (false);
		go_animations7.gameObject.SetActive (false);
		go_animations8.gameObject.SetActive (false);
		go_animations9.gameObject.SetActive (false);
		go_animations10.gameObject.SetActive (false);
	}

	/// <summary>
	/// Sets the active animations.
	/// </summary>
	public void SetActiveAnimations (int iFeedbackNumber)
	{
		// Verifica qual eh o numero da tela do Feedback.
		switch(iFeedbackNumber)
		{
		// Caso seja o feedback 1.
		case (1):

			// ATIVA as animacoes do Feedback 1.
			go_animations1.SetActive (true);
			
			break;

			// Caso seja o feedback 2.
		case (2):
			
			// ATIVA as animacoes do Feedback 2.
			go_animations2.SetActive (true);
			
			break;

			// Caso seja o feedback 3.
		case (3):
			
			// ATIVA as animacoes do Feedback 3.
			go_animations3.SetActive (true);
			
			break;

			// Caso seja o feedback 4.
		case (4):
			
			// ATIVA as animacoes do Feedback 4.
			go_animations4.SetActive (true);
			
			break;

			// Caso seja o feedback 5.
		case (5):
			
			// ATIVA as animacoes do Feedback 5.
			go_animations5.SetActive (true);
			
			break;

			// Caso seja o feedback 6.
		case (6):
			
			// ATIVA as animacoes do Feedback 6.
			go_animations6.SetActive (true);
			
			break;

			// Caso seja o feedback 7.
		case (7):
			
			// ATIVA as animacoes do Feedback 7.
			go_animations7.SetActive (true);
			
			break;

			// Caso seja o feedback 8.
		case (8):
			
			// ATIVA as animacoes do Feedback 8.
			go_animations8.SetActive (true);
			
			break;

			// Caso seja o feedback 9.
		case (9):
			
			// ATIVA as animacoes do Feedback 9.
			go_animations9.SetActive (true);
			
			break;

			// Caso seja o feedback 10.
		case (10):
			
			// ATIVA as animacoes do Feedback 10.
			go_animations10.SetActive (true);
			
			break;

		default:

			break;
		}
	}

	/// <summary>
	/// Sets the deactivate animations.
	/// </summary>
	/// <param name="iFeedbackNumber">I feedback number.</param>
	public void SetDeactivateAnimations (int iFeedbackNumber)
	{
		
		// Verifica qual eh o numero da tela do Feedback.
		switch(iFeedbackNumber)
		{
			// Caso seja o feedback 1.
		case (1):
			
			// ATIVA as animacoes do Feedback 1.
			go_animations1.SetActive (false);
			
			break;
			
			// Caso seja o feedback 2.
		case (2):
			
			// ATIVA as animacoes do Feedback 2.
			go_animations2.SetActive (false);
			
			break;
			
			// Caso seja o feedback 3.
		case (3):
			
			// ATIVA as animacoes do Feedback 3.
			go_animations3.SetActive (false);
			
			break;
			
			// Caso seja o feedback 4.
		case (4):
			
			// ATIVA as animacoes do Feedback 4.
			go_animations4.SetActive (false);
			
			break;
			
			// Caso seja o feedback 5.
		case (5):
			
			// ATIVA as animacoes do Feedback 5.
			go_animations5.SetActive (false);
			
			break;
			
			// Caso seja o feedback 6.
		case (6):
			
			// ATIVA as animacoes do Feedback 6.
			go_animations6.SetActive (false);
			
			break;
			
			// Caso seja o feedback 7.
		case (7):
			
			// ATIVA as animacoes do Feedback 7.
			go_animations7.SetActive (false);
			
			break;
			
			// Caso seja o feedback 8.
		case (8):
			
			// ATIVA as animacoes do Feedback 8.
			go_animations8.SetActive (false);
			
			break;
			
			// Caso seja o feedback 9.
		case (9):
			
			// ATIVA as animacoes do Feedback 9.
			go_animations9.SetActive (false);
			
			break;
			
			// Caso seja o feedback 10.
		case (10):
			
			// ATIVA as animacoes do Feedback 10.
			go_animations10.SetActive (false);
			
			break;
			
		default:
			
			break;
		}
	}
}
