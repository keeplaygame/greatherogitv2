﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Initial loading.
/// Esse script serve para identificar qual é o aspect ratio do device do usuário e encaminhá-lo para a scene correspondente ao aspect ratio identificado.
/// Esse script deve ser utilizado em uma scene que antecede as scenes do jogo propriamente dito, ou seja, ele deve estar em uma scene após a Splash obrigatória da Unity.
/// </summary>

public class InitialLoading : MonoBehaviour 
{
	void Start () 
	{	
		// Deleta a chave que indica que a splash já foi vista
		PlayerPrefs.DeleteKey("iIsSplashShowed");

		// Deleta a chave que indica que deve mostrar a galeria de selos
		PlayerPrefs.DeleteKey("iIsToShowStampGallery");

		// Deleta a chave que indica se o som está ligado
		PlayerPrefs.DeleteKey("iIsAudioOn");

		// Deleta a chave que indica que os produtos da loja foram guardados
		PlayerPrefs.DeleteKey("iIsProductsRequested");

		// Deleta a chave que indica que deve ser iniciada a compra das fases
		PlayerPrefs.DeleteKey("iIsToStartLevelsPurchase");

		// Manda rodar função para setar qual cena carregar.
		SetScene();
	}	

	/// <summary>
	/// Sets the scene.
	/// </summary>
	private void SetScene ()
	{
		// Envia para a Main Scene
		SceneController.LoadLevelMainSceneWithoutShowActivity();
	}
}