﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour 
{
	// Recebe e guarda GameObject que tem o script SFXCONTROLLER.
	public SFXController sfxController;

	/// <summary>
	/// Sets the screen.
	/// </summary>
	public void SetAction (string strActionName)
	{
		// Verifica qual eh o Nome da Acao.
		switch (strActionName)
		{				
			// Caso seja AudioOn.
		case "AudioOn":
			
			// Toca o som de botao apertado.
			// audio.PlayOneShot(audio_button, 0.3f);
			SetPlaySFXAudio (0.3f);
			
			// Seta o volume igual a 1.
			AudioListener.volume = 1;

			break;
			
			// Caso seja AudioOff.
		case "AudioOff":
			
			// Seta o volume igual a 0
			AudioListener.volume = 0;

			break;

		default:

			break;
		}		
	}

	/// <summary>
	/// Sets the play SFX audio.
	/// </summary>
	public void SetPlaySFXAudio (float fVolume)
	{
		// Executa função no SFXController para tocar áudio.
		sfxController.SetPlayAudioButton(fVolume);
	}
}