﻿using UnityEngine;
using System.Collections;

public class Modulo10Controller : MonoBehaviour 
{
	// Recebe o screen controller do jogo
	public ScreenController_episode screenController_episode;

	[SerializeField]
	// Guarda a camera do modulo.
	private GameObject go_camModulo10;

	[SerializeField]
	// Nome da chave do selo que será gravada no PlayerPref
	private string str_stampPlayerPrefKey;

	// Indica se os itens a arrastar estao no lugar certo
	private bool[] array_b_isItemAtRightPlace = new bool[4];

	// Indica se estah reiniciando a tela
	public bool b_isRestartingScreen;

	// Indica se completou a fase e nao pode mais arrastar
	private bool b_isVictoryCantDrag;

	// Recebe e guarda o GameObject que tem o MusicController Script.
	public MusicController musicController;

	// Use this for initialization
	void Start () 
	{
		// Seta que nao estah reiniciando a tela
		b_isRestartingScreen = false;

		// Seta que a fase nao foi completada
		b_isVictoryCantDrag = false;

		// Percorre todos os itens a serem arrastados
		for (int i = 0; i < array_b_isItemAtRightPlace.Length; i++)
		{
			// Seta que o item nao estah no lugar certo
			array_b_isItemAtRightPlace[i] = false;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Verifica se deve reiniciar a tela
		CheckRestartScreen();

		// Verifica se completou
		CheckVictory();
	}

	/// <summary>
	/// Sets the item at right place.
	/// </summary>
	/// <param name="iItemNumber">I item number.</param>
	/// <param name="bValue">If set to <c>true</c> b value.</param>
	public void SetItemAtRightPlace (int iItemNumber, bool bValue)
	{
		// Seta se o item estah ou nao no lugar certo
		array_b_isItemAtRightPlace[iItemNumber] = bValue;
	}

	/// <summary>
	/// Determines whether this instance is restarting modulo.
	/// </summary>
	/// <returns><c>true</c> if this instance is restarting modulo1; otherwise, <c>false</c>.</returns>
	public bool IsRestartingModulo ()
	{
		// Retorna se estah reiniciando a tela
		return b_isRestartingScreen;
	}

	/// <summary>
	/// Sets the restarted modulo.
	/// </summary>
	public void SetRestartedModulo ()
	{
		// Seta que jah reiniciou a tela
		b_isRestartingScreen = false;
	}

	/// <summary>
	/// Checks the restart screen.
	/// </summary>
	private void CheckRestartScreen ()
	{
		// Verifica se vai reiniciar a tela
		if (screenController_episode.IsGoingToRestartModulo10())
		{
			// Seta que estah reiniciando a tela
			screenController_episode.SetRestartingModulo10();

			// Seta que estah reiniciando a tela
			b_isRestartingScreen = true;
		}

		// Verifica se estah reiniciando a tela
		if (b_isRestartingScreen)
		{
			// Verifica se todos os itens estao no lugar inicial
			if (!array_b_isItemAtRightPlace[0] && !array_b_isItemAtRightPlace[1] && !array_b_isItemAtRightPlace[2] && !array_b_isItemAtRightPlace[3])
			{
				// Seta que nao estah encerrando a fase por vitoria
				b_isVictoryCantDrag = false;

				// Seta que reiniciou a tela
				b_isRestartingScreen = false;
			}
		}
	}

	/// <summary>
	/// Checks the victory.
	/// </summary>
	private void CheckVictory()
	{
		// Verifica se nao estah terminando a tela com vitoria e estah na camera do modulo
		if (!b_isVictoryCantDrag && go_camModulo10.activeInHierarchy)
		{
			// Verifica se todos os itens estao na posicao correta
			if (array_b_isItemAtRightPlace[0] && array_b_isItemAtRightPlace[1] && array_b_isItemAtRightPlace[2] && array_b_isItemAtRightPlace[3])
			{
				// Seta que estah terminando a fase por vitoria
				b_isVictoryCantDrag = true;
					
				// Inicia a co-rotina para encerrar a fase
				StartCoroutine(WaitTimeToFinishModulo());
			}
		}
	}

	/// <summary>
	/// Waits the time to finish modulo.
	/// </summary>
	/// <returns>The time to finish modulo.</returns>
	IEnumerator WaitTimeToFinishModulo()
	{
		// Envia para o GameObject MusicController que eh para tocar a musica de VITORIA.
		musicController.SetMusic ("Victory");

		// Desabilita os botoes da tela
		screenController_episode.SetScreenButtonsNotActive();

		// Aguarda determinado tempo para encerrar a fase
		yield return new WaitForSeconds (2.5f);

		// Verifica se o jogador tem menos do que 10 selos
		if (PlayerPrefs.GetInt(str_stampPlayerPrefKey + "Quantity", 0) < 10)
		{
			// Adiciona um selo ganho
			PlayerPrefs.SetInt(str_stampPlayerPrefKey + "Quantity", PlayerPrefs.GetInt(str_stampPlayerPrefKey + "Quantity", 0) + 1); 
		}

		// Adiciona um selo a nova quantidade para indicar que precisa de animação
		PlayerPrefs.SetInt(str_stampPlayerPrefKey + "NewQuantity", PlayerPrefs.GetInt(str_stampPlayerPrefKey + "NewQuantity", 0) + 1);

		// Finaliza a tela atual e vai para a proxima
		screenController_episode.FinishModulo10Screen();
		
		// Reinicia a tela atual
		b_isRestartingScreen = true;
	}
}
