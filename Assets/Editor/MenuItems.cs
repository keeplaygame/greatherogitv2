﻿using UnityEngine;
using UnityEditor;

public class MenuItems
{
	// Cria um dropdown menu button na barra de Menu da Unity com o nome Keeplay/Clear Player Prefs.
	[MenuItem("Keeplay/Clear PlayerPrefs")]

	// Cria uma funcao static para o botao.
	private static void DeleteAllPlayerPrefs()
	{
		// Deleta os PlayerPrefs.
		PlayerPrefs.DeleteAll();

		// Imprime mensagem no Console.
		Debug.Log ("PlayerPref DELETADOS");
	}
}