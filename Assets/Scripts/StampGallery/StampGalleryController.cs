﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class StampGalleryController : MonoBehaviour 
{
	[SerializeField]
	// Guarda a quantidade dos selos
	private Text[] array_text_StampQuantity = new Text[10];

	// Guarda o nome dos selos
	private string[] array_str_stampName = new string[10];

	[SerializeField]
	// Guarda os objetos dos numeros dos selos
	private GameObject[] array_go_stampNumber = new GameObject[10];

	[SerializeField]
	// Recebe o sfxController
	private SFXController sfxController;

	// Use this for initialization
	void Awake () 
	{
		// Seta os nomes dos selos
		array_str_stampName[0] = "PurityStamp";
		array_str_stampName[1] = "ConfidenceStamp";
		array_str_stampName[2] = "HumblenessStamp";
		array_str_stampName[3] = "DevotionStamp ";
		array_str_stampName[4] = "JusticeStamp";
		array_str_stampName[5] = "BraveryStamp";
		array_str_stampName[6] = "ObedienceStamp";
		array_str_stampName[7] = "FriendshipStamp";
		array_str_stampName[8] = "KindnessStamp";
		array_str_stampName[9] = "LoveStamp";
	}
	
	/// <summary>
	/// Restarts the stamp gallery.
	/// </summary>
	public void RestartStampGallery ()
	{
		// Percorre todos os selos
		for (int i = 0; i < array_str_stampName.Length; i++)
		{
			// Verifica se ganhou um novo selo
			if (PlayerPrefs.GetInt(array_str_stampName[i] + "NewQuantity", 0) > 0)
			{
				// Verifica se a quantidade atual é maior que 0 e menor que 10
				if (PlayerPrefs.GetInt(array_str_stampName[i] + "Quantity", 0) > 0 && PlayerPrefs.GetInt(array_str_stampName[i] + "Quantity", 0) < 10)
				{
					// Seta o texto da quantidade de selos
					array_text_StampQuantity[i].text = (PlayerPrefs.GetInt(array_str_stampName[i] + "Quantity", 0)-1).ToString();
				}

				// A quantidade atual é 0
				else
				{
					// Seta o texto da quantidade de selos
					array_text_StampQuantity[i].text = PlayerPrefs.GetInt(array_str_stampName[i] + "Quantity", 0).ToString();
				}

				// Toca a animação de ganhar um selo
				array_go_stampNumber[i].GetComponent<Animator>().Play("StampTextQuantityIncrease");

				// Inicia a corotina para mudar a quantidade do selo
				StartCoroutine("ChangeQuantity", i);

				// Seta que não tem mais novo selo
				PlayerPrefs.SetInt(array_str_stampName[i] + "NewQuantity", 0);
			}

			// Não ganhou um novo selo
			else
			{
				// Seta o texto da quantidade de selos
				array_text_StampQuantity[i].text = PlayerPrefs.GetInt(array_str_stampName[i] + "Quantity", 0).ToString();
			}
		}
	}

	/// <summary>
	/// Changes the quantity.
	/// </summary>
	/// <returns>The quantity.</returns>
	/// <param name="iStampNumber">I stamp number.</param>
	private IEnumerator ChangeQuantity (int iStampNumber)
	{
		// Aguarda meio segundo para incrementar a quantidade do selo
		yield return new WaitForSeconds(0.5f);

		// Toca o som de novo selo
		sfxController.SetPlayAudioNewStamp(1.0f);

		// Atualiza o texto com a quantidade certa
		array_text_StampQuantity[iStampNumber].text = array_text_StampQuantity[iStampNumber].text = PlayerPrefs.GetInt(array_str_stampName[iStampNumber] + "Quantity", 0).ToString();
	}
}
