﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ScreenController_episode2 : MonoBehaviour 
{
	// Enumerador com todas as telas do jogo
	enum Screens
	{
		TelaFases,
		Modulo1,
		FeedbackModulo1,
		Modulo2,
		FeedbackModulo2,
		Modulo3,
		FeedbackModulo3,
		Modulo4,
		FeedbackModulo4,
		Modulo5,
		FeedbackModulo5,
		Modulo6,
		FeedbackModulo6,
		Modulo7,
		FeedbackModulo7,
		Modulo8,
		FeedbackModulo8,
		Modulo9,
		FeedbackModulo9,
		Modulo10,
		FeedbackModulo10,
	}

	private Screens enum_actualScreen; // Indica a tela atual do jogo
	private Screens enum_lastScreen; // Indica a tela anterior

	//
	// CAMERAS Inicio.
	//
	public GameObject go_fases; // Camera da tela fases.
	public GameObject go_modulo1; // Camera da tela modulo1
	public GameObject go_feedbackModulo1; // Camera da tela  de feedback do modulo1
	public GameObject go_modulo2; // Camera da tela modulo2
	public GameObject go_feedbackModulo2; // Camera da tela  de feedback do modulo2
	public GameObject go_modulo3; // Camera da tela modulo3
	public GameObject go_feedbackModulo3; // Camera da tela  de feedback do modulo3
	public GameObject go_modulo4; // Camera da tela modulo4
	public GameObject go_feedbackModulo4; // Camera da tela  de feedback do modulo4
	public GameObject go_modulo5; // Camera da tela modulo5
	public GameObject go_feedbackModulo5; // goera da tela  de feedback do modulo5
	public GameObject go_modulo6; // Camera da tela modulo6
	public GameObject go_feedbackModulo6; // Camera da tela  de feedback do modulo6
	public GameObject go_modulo7; // Camera da tela modulo7
	public GameObject go_feedbackModulo7; // Camera da tela  de feedback do modulo7
	public GameObject go_modulo8; // Camera da tela modulo8
	public GameObject go_feedbackModulo8; // Camera da tela  de feedback do modulo8
	public GameObject go_modulo9; // Camera da tela modulo9
	public GameObject go_feedbackModulo9; // Camera da tela  de feedback do modulo9
	public GameObject go_modulo10; // Camera da tela modulo10
	public GameObject go_feedbackModulo10; // Camera da tela  de feedback do modulo10
	//
	// CAMERAS Fim.
	//

	private bool b_isGoingToRestartModulo1; // Indica se vai reiniciar o modulo1.
	private bool b_isGoingToRestartModulo2; // Indica se vai reiniciar o modulo2.
	private bool b_isGoingToRestartModulo3; // Indica se vai reiniciar o modulo3.
	private bool b_isGoingToRestartModulo4; // Indica se vai reiniciar o modulo4.
	private bool b_isGoingToRestartModulo5; // Indica se vai reiniciar o modulo5.
	private bool b_isGoingToRestartModulo6; // Indica se vai reiniciar o modulo6.
	private bool b_isGoingToRestartModulo7; // Indica se vai reiniciar o modulo7.
	private bool b_isGoingToRestartModulo8; // Indica se vai reiniciar o modulo8.
	private bool b_isGoingToRestartModulo9; // Indica se vai reiniciar o modulo9.
	private bool b_isGoingToRestartModulo10; // Indica se vai reiniciar o modulo10.
	private bool b_isChangingScreen; // Indica se estah mudando de camera.
	public bool b_isScreenButtonsActive; // Indica se os botoes da tela estao ativos.
	public bool b_isAtLevelScreen; // Indica se estah na tela de fases.

	// Recebe e guarda o  gameobject do canvas da scene
	private GameObject go_sceneCanvas;

	// Recebe e guarda o FEEDBACKS CONTROLLER.
	public FeedbacksController feedbacksController;

	// Recebe e guarda o GameObject que tem o AudioController Script.
	public AudioController audioController;

	// Recebe e guarda o GameObject que tem o MusicController Script.
	public MusicController musicController;

	// Recebe o level controller do episodio
	public LevelController levelController;

	[SerializeField]
	// Recebe o controlador de compras
	private InAppController inAppController;

	void Awake ()
	{
		// Configuração do canvas
		CanvasConfiguration();

		// Para o indicador de atividade
		LoadingController.HideActivity();
	}

	// Use this for initialization
	void Start () 
	{
		// Inicia na tela de splash 01
		enum_actualScreen = Screens.TelaFases; 

		// Seta que nao estah mudando de tela.
		b_isChangingScreen = false;

		// Seta que os botoes da tela estao ativos.
		b_isScreenButtonsActive = true; 

		// Seta que nao vai reiniciar o modulo 1.
		b_isGoingToRestartModulo1 = false;

		// Seta que nao vai reiniciar o modulo 2.
		b_isGoingToRestartModulo2 = false;

		// Seta que nao vai reiniciar o modulo 3.
		b_isGoingToRestartModulo3 = false;

		// Seta que nao vai reiniciar o modulo 4.
		b_isGoingToRestartModulo4 = false;

		// Seta que nao vai reiniciar o modulo 5.
		b_isGoingToRestartModulo5 = false;

		// Seta que nao vai reiniciar o modulo 6.
		b_isGoingToRestartModulo6 = false;

		// Seta que nao vai reiniciar o modulo 7.
		b_isGoingToRestartModulo7 = false;

		// Seta que nao vai reiniciar o modulo 8.
		b_isGoingToRestartModulo8 = false;

		// Seta que nao vai reiniciar o modulo 9.
		b_isGoingToRestartModulo9 = false;

		// Seta que nao vai reiniciar o modulo 10.
		b_isGoingToRestartModulo10 = false;

		// Seta que nao estah na tela de fases.
		b_isAtLevelScreen = false;

//		SetAction ("TelaFases");

		// EXECUTA função para o LANGUAGE.CS atualizar o idioma da tela. 
		Language.SwitchLanguage(Language.CurrentLanguage());
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Verifica se tem mudança de tela
		CheckChangeScreen();
	}

	/// <summary>
	/// Determines whether this instance canvas configuration.
	/// </summary>
	/// <returns><c>true</c> if this instance canvas configuration; otherwise, <c>false</c>.</returns>
	void CanvasConfiguration()
	{
		// Guarda o gameobject do canvas
		go_sceneCanvas = GameObject.Find("Canvas");

		// Guarda o canvas scaler do canvas
		CanvasScaler canvasScaler = go_sceneCanvas.GetComponent<CanvasScaler>();

		// Proporção do tamanho da tela
		float fAspectRatio;

		Debug.Log("largura da tela: " +  Screen.width);
		Debug.Log("altura da tela: " +  Screen.height);

		// Verifica se a largura é maior ou igual que a altura
		if (Screen.width >= Screen.height)
		{
			// Calculo do aspectRatio
			fAspectRatio = (float)Screen.width/Screen.height;
		}

		// A altura é maior
		else
		{
			// Calculo do aspectRatio
			fAspectRatio = (float)Screen.height/Screen.width;
		}

		Debug.Log("proporção: " + fAspectRatio);

		// Verifica se o aspect ratio é menos wide que a referência (Referência: 2208:1536)
		if (fAspectRatio < 1.4375f)
		{
			canvasScaler.matchWidthOrHeight = 1;
		}

		else if (fAspectRatio > 1.4375f)
		{
			canvasScaler.matchWidthOrHeight = 0;
		} 

		else if (fAspectRatio == 1.4375f)
		{
			canvasScaler.matchWidthOrHeight = 0.5f;
		}


		Debug.Log("fim");

	}

	/// <summary>
	/// Checks the change screen.
	/// </summary>
	void CheckChangeScreen ()
	{
		// Verifica se estah mudando de tela
		if (b_isChangingScreen)
		{

			// Seta que mudou de tela
			b_isChangingScreen = false; 

			// Desabilita todas as cameras
			go_fases.gameObject.SetActive(false);
			go_modulo1.gameObject.SetActive(false);
			go_feedbackModulo1.gameObject.SetActive(false);
			go_modulo2.gameObject.SetActive(false);
			go_feedbackModulo2.gameObject.SetActive(false);
			go_modulo3.gameObject.SetActive(false);
			go_feedbackModulo3.gameObject.SetActive(false);
			go_modulo4.gameObject.SetActive(false);
			go_feedbackModulo4.gameObject.SetActive(false);
			go_modulo5.gameObject.SetActive(false);
			go_feedbackModulo5.gameObject.SetActive(false);
			go_modulo6.gameObject.SetActive(false);
			go_feedbackModulo6.gameObject.SetActive(false);
			go_modulo7.gameObject.SetActive(false);
			go_feedbackModulo7.gameObject.SetActive(false);
			go_modulo8.gameObject.SetActive(false);
			go_feedbackModulo8.gameObject.SetActive(false);
			go_modulo9.gameObject.SetActive(false);
			go_feedbackModulo9.gameObject.SetActive(false);
			go_modulo10.gameObject.SetActive(false);
			go_feedbackModulo10.gameObject.SetActive(false);

			// Verifica qual eh a tela atual
			switch (enum_actualScreen)
			{
				// Caso seja a tela de fases
			case Screens.TelaFases:
				
				// Habilita a camera da tela de configuracoes
				go_fases.gameObject.SetActive(true);

				// Atualiza as fases abertas
				levelController.UpdateLevels();

				// Verifica se deve iniciar a compra das fases
				if (PlayerPrefs.GetInt("iIsToStartLevelsPurchase", 0) == 1)
				{
					//
					// TROCAR quando for usar a conta oficial da Mount Moriah
					//

					#if UNITY_IPHONE
						// Inicia a compra das fases
						inAppController.PurchaseProduct(levelController.str_episodeName + "Levels");
					#endif

					// Deleta a chave que deve iniciar a compra das fases
					PlayerPrefs.DeleteKey("iIsToStartLevelsPurchase");
				}

				break;

				// Caso seja a tela Modulo 1
			case Screens.Modulo1:

				// Habilita a tela de modulo 1
				go_modulo1.gameObject.SetActive(true);

				break;

				// Caso seja a tela de Feedback do Modulo 1
			case Screens.FeedbackModulo1:
				
				// Habilita a tela de feedback do modulo 1
				go_feedbackModulo1.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 2
			case Screens.Modulo2:
				
				// Habilita a tela de modulo 2
				go_modulo2.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela de Feedback do Modulo 2
			case Screens.FeedbackModulo2:
				
				// Habilita a tela de feedback do modulo 2
				go_feedbackModulo2.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 3
			case Screens.Modulo3:
				
				// Habilita a tela de modulo 3
				go_modulo3.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela de Feedback do Modulo 3
			case Screens.FeedbackModulo3:
				
				// Habilita a tela de feedback do modulo 3
				go_feedbackModulo3.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 4
			case Screens.Modulo4:
				
				// Habilita a tela de modulo 4
				go_modulo4.gameObject.SetActive(true);
				
				break;
				
				// Caso seja a tela de Feedback do Modulo 4
			case Screens.FeedbackModulo4:
				
				// Habilita a tela de feedback do modulo 4
				go_feedbackModulo4.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 5
			case Screens.Modulo5:
				
				// Habilita a tela de modulo 5
				go_modulo5.gameObject.SetActive(true);
				
				break;
				
				// Caso seja a tela de Feedback do Modulo 5
			case Screens.FeedbackModulo5:
				
				// Habilita a tela de feedback do modulo 5
				go_feedbackModulo5.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 6
			case Screens.Modulo6:
				
				// Habilita a tela de modulo 6
				go_modulo6.gameObject.SetActive(true);
				
				break;
				
				// Caso seja a tela de Feedback do Modulo 6
			case Screens.FeedbackModulo6:
				
				// Habilita a tela de feedback do modulo 6
				go_feedbackModulo6.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 7
			case Screens.Modulo7:
				
				// Habilita a tela de modulo 7
				go_modulo7.gameObject.SetActive(true);
				
				break;
				
				// Caso seja a tela de Feedback do Modulo 7
			case Screens.FeedbackModulo7:
				
				// Habilita a tela de feedback do modulo 7
				go_feedbackModulo7.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 8
			case Screens.Modulo8:
				
				// Habilita a tela de modulo 8
				go_modulo8.gameObject.SetActive(true);
				
				break;
				
				// Caso seja a tela de Feedback do Modulo 8
			case Screens.FeedbackModulo8:
				
				// Habilita a tela de feedback do modulo 8
				go_feedbackModulo8.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 9
			case Screens.Modulo9:
				
				// Habilita a tela de modulo 9
				go_modulo9.gameObject.SetActive(true);
				
				break;
				
				// Caso seja a tela de Feedback do Modulo 9
			case Screens.FeedbackModulo9:
				
				// Habilita a tela de feedback do modulo 9
				go_feedbackModulo9.gameObject.SetActive(true);
				
				break;

				// Caso seja a tela Modulo 10
			case Screens.Modulo10:
				
				// Habilita a tela de modulo 10.
				go_modulo10.gameObject.SetActive(true);
				
				break;
				
				// Caso seja a tela de Feedback do Modulo 10
			case Screens.FeedbackModulo10:
				
				// Habilita a tela de feedback do modulo 10
				go_feedbackModulo10.gameObject.SetActive(true);
				
				break;

			default:
				
				break;
			}

			// EXECUTA função para o LANGUAGE.CS atualizar o idioma da tela. 
			Language.SwitchLanguage(Language.CurrentLanguage());

			// Seta que os botoes estah ativos
			b_isScreenButtonsActive = true;
			
			// Usar aqui?? Mas não é assincrono?
			Resources.UnloadUnusedAssets();
		}
	}

	/// <summary>
	/// Determines whether this instance is going to restart modulo1.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo1; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo1 ()
	{
		// Retorna se vai reinicar o modulo 1
		return b_isGoingToRestartModulo1;
	}

	/// <summary>
	/// Sets the restarting modulo1.
	/// </summary>
	public void SetRestartingModulo1 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo1 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 1.
		feedbacksController.SetDeactivateAnimations(1);
	}

	/// <summary>
	/// Finishs the modulo1 screen.
	/// </summary>
	public void FinishModulo1Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 1
		enum_actualScreen = Screens.FeedbackModulo1;

		// Seta que a fase 0 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(0);

		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 1.
		feedbacksController.SetActiveAnimations(1);
	}

	/// <summary>
	/// Determines whether this instance is going to restart modulo2.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo2; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo2 ()
	{
		// Retorna se vai reinicar o modulo 2
		return b_isGoingToRestartModulo2;
	}

	/// <summary>
	/// Sets the restarting modulo2.
	/// </summary>
	public void SetRestartingModulo2 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo2 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 2.
		feedbacksController.SetDeactivateAnimations(2);
	}

	/// <summary>
	/// Finishs the modulo2 screen.
	/// </summary>
	public void FinishModulo2Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 2
		enum_actualScreen = Screens.FeedbackModulo2;

		// Seta que a fase 1 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(1);
		
		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 2.
		feedbacksController.SetActiveAnimations(2);
	}

	#region modulo 3
	/// <summary>
	/// Determines whether this instance is going to restart modulo3.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo3; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo3 ()
	{
		// Retorna se vai reinicar o modulo 3
		return b_isGoingToRestartModulo3;
	}
	
	/// <summary>
	/// Sets the restarting modulo3.
	/// </summary>
	public void SetRestartingModulo3 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo3 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 3.
		feedbacksController.SetDeactivateAnimations(3);
	}

	/// <summary>
	/// Finishs the modulo3 screen.
	/// </summary>
	public void FinishModulo3Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 3
		enum_actualScreen = Screens.FeedbackModulo3;

		// Seta que a fase 2 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(2);

		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 3.
		feedbacksController.SetActiveAnimations(3);
	}
	#endregion

	#region modulo 4
	/// <summary>
	/// Determines whether this instance is going to restart modulo4.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo4; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo4 ()
	{
		// Retorna se vai reinicar o modulo 4
		return b_isGoingToRestartModulo4;
	}
	
	/// <summary>
	/// Sets the restarting modulo4.
	/// </summary>
	public void SetRestartingModulo4 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo4 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 4.
		feedbacksController.SetDeactivateAnimations(4);
	}
	
	/// <summary>
	/// Finishs the modulo4 screen.
	/// </summary>
	public void FinishModulo4Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 4
		enum_actualScreen = Screens.FeedbackModulo4;

		// Seta que a fase 3 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(3);
		
		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 4.
		feedbacksController.SetActiveAnimations(4);
	}
	#endregion

	#region modulo 5
	/// <summary>
	/// Determines whether this instance is going to restart modulo5.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo5; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo5 ()
	{
		// Retorna se vai reinicar o modulo 5
		return b_isGoingToRestartModulo5;
	}
	
	/// <summary>
	/// Sets the restarting modulo5.
	/// </summary>
	public void SetRestartingModulo5 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo5 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 5.
		feedbacksController.SetDeactivateAnimations(5);
	}
	
	/// <summary>
	/// Finishs the modulo5 screen.
	/// </summary>
	public void FinishModulo5Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 5
		enum_actualScreen = Screens.FeedbackModulo5;

		// Seta que a fase 4 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(4);
		
		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 5.
		feedbacksController.SetActiveAnimations(5);
	}
	#endregion

	#region modulo 6
	/// <summary>
	/// Determines whether this instance is going to restart modulo6.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo6; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo6 ()
	{
		// Retorna se vai reinicar o modulo 6
		return b_isGoingToRestartModulo6;
	}
	
	/// <summary>
	/// Sets the restarting modulo6.
	/// </summary>
	public void SetRestartingModulo6 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo6 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 6.
		feedbacksController.SetDeactivateAnimations(6);
	}
	
	/// <summary>
	/// Finishs the modulo6 screen.
	/// </summary>
	public void FinishModulo6Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 6
		enum_actualScreen = Screens.FeedbackModulo6;

		// Seta que a fase 5 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(5);
		
		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 6.
		feedbacksController.SetActiveAnimations(6);
	}
	#endregion

	#region modulo 7
	/// <summary>
	/// Determines whether this instance is going to restart modulo7.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo7; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo7 ()
	{
		// Retorna se vai reinicar o modulo 7
		return b_isGoingToRestartModulo7;
	}
	
	/// <summary>
	/// Sets the restarting modulo7.
	/// </summary>
	public void SetRestartingModulo7 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo7 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 7.
		feedbacksController.SetDeactivateAnimations(7);
	}
	
	/// <summary>
	/// Finishs the modulo7 screen.
	/// </summary>
	public void FinishModulo7Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 7
		enum_actualScreen = Screens.FeedbackModulo7;

		// Seta que a fase 6 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(6);
		
		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 7.
		feedbacksController.SetActiveAnimations(7);
	}
	#endregion

	#region modulo 8
	/// <summary>
	/// Determines whether this instance is going to restart modulo8.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo8; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo8 ()
	{
		// Retorna se vai reinicar o modulo 8
		return b_isGoingToRestartModulo8;
	}
	
	/// <summary>
	/// Sets the restarting modulo8.
	/// </summary>
	public void SetRestartingModulo8 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo8 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 8.
		feedbacksController.SetDeactivateAnimations(8);
	}
	
	/// <summary>
	/// Finishs the modulo8 screen.
	/// </summary>
	public void FinishModulo8Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 8
		enum_actualScreen = Screens.FeedbackModulo8;

		// Seta que a fase 7 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(7);
		
		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 8.
		feedbacksController.SetActiveAnimations(8);
	}
	#endregion

	#region modulo 9
	/// <summary>
	/// Determines whether this instance is going to restart modulo9.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo9; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo9 ()
	{
		// Retorna se vai reinicar o modulo 9
		return b_isGoingToRestartModulo9;
	}
	
	/// <summary>
	/// Sets the restarting modulo9.
	/// </summary>
	public void SetRestartingModulo9 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo9 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 9.
		feedbacksController.SetDeactivateAnimations(9);
	}
	
	/// <summary>
	/// Finishs the modulo9 screen.
	/// </summary>
	public void FinishModulo9Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 9
		enum_actualScreen = Screens.FeedbackModulo9;

		// Seta que a fase 8 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(8);
		
		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 9.
		feedbacksController.SetActiveAnimations(9);
	}
	#endregion

	#region modulo 10
	/// <summary>
	/// Determines whether this instance is going to restart modulo9.
	/// </summary>
	/// <returns><c>true</c> if this instance is going to restart modulo9; otherwise, <c>false</c>.</returns>
	public bool IsGoingToRestartModulo10 ()
	{
		// Retorna se vai reinicar o modulo 10
		return b_isGoingToRestartModulo10;
	}
	
	/// <summary>
	/// Sets the restarting modulo10.
	/// </summary>
	public void SetRestartingModulo10 ()
	{
		// Seta que jah estah reiniciando a tela
		b_isGoingToRestartModulo10 = false;

		// Manda o feedbacks controller DESATIVAR as animacoes da tela do Feedback 10.
		feedbacksController.SetDeactivateAnimations(10);
	}
	
	/// <summary>
	/// Finishs the modulo10 screen.
	/// </summary>
	public void FinishModulo10Screen ()
	{
		// Guarda a tela anterior
		enum_lastScreen = enum_actualScreen;
		
		// Carrega a tela de feedback modulo 10
		enum_actualScreen = Screens.FeedbackModulo10;

		// Seta que a fase 9 do episódio foi completada - A primeira fase é a 0
		levelController.SetLevelCompleted(9);
		
		// Seta que estah mudando de tela
		b_isChangingScreen = true;

		// Manda o feedbacks controller ativar as animacoes da tela do Feedback 10.
		feedbacksController.SetActiveAnimations (10);
	}
	#endregion

	/// <summary>
	/// Sets the screen buttons not active.
	/// </summary>
	public void SetScreenButtonsNotActive ()
	{
		// Seta que os botoes da tela nao estao ativos
		b_isScreenButtonsActive = false;
	}

	/// <summary>
	/// Feeds the back restart button.
	/// </summary>
	/// <param name="iCurrentModulo">I current modulo.</param>
	private void FeedBackRestartButton (int iCurrentModulo)
	{
		// Verifica se os botoes da tela estah ativos.
		if (b_isScreenButtonsActive)
		{
			// Toca o som de botao apertado.
			// audio.PlayOneShot(audio_button, 0.3f);
			audioController.SetPlaySFXAudio (0.3f);

			// Guarda a tela anterior
			enum_lastScreen = enum_actualScreen;

			// Verifica o modulo atual
			switch (iCurrentModulo)
			{
				// Caso seja a fase 1
			case 1:

				// Carrega a tela do modulo 1
				enum_actualScreen = Screens.Modulo1; 
				
				// Seta que vai reiniciar o modulo 1
				b_isGoingToRestartModulo1 = true;

				break;

				// Caso seja a fase 2
			case 2:

				// Carrega a tela do modulo 2
				enum_actualScreen = Screens.Modulo2; 
				
				// Seta que vai reiniciar o modulo 2
				b_isGoingToRestartModulo2 = true;

				break;

				// Caso seja a fase 3
			case 3:

				// Carrega a tela do modulo 3
				enum_actualScreen = Screens.Modulo3; 
				
				// Seta que vai reiniciar o modulo 3
				b_isGoingToRestartModulo3 = true;

				break;

				// Caso seja a fase 4
			case 4:

				// Carrega a tela do modulo 4
				enum_actualScreen = Screens.Modulo4; 
				
				// Seta que vai reiniciar o modulo 4
				b_isGoingToRestartModulo4 = true;

				break;

				// Caso seja a fase 5
			case 5:

				// Carrega a tela do modulo 5
				enum_actualScreen = Screens.Modulo5;
				
				// Seta que vai reiniciar o modulo 5
				b_isGoingToRestartModulo5 = true;

				break;

				// Caso seja a fase 6
			case 6:

				// Carrega a tela do modulo 6
				enum_actualScreen = Screens.Modulo6; 
				
				// Seta que vai reiniciar o modulo 6
				b_isGoingToRestartModulo6 = true;

				break;

				// Caso seja a fase 7
			case 7:

				// Carrega a tela do modulo 7
				enum_actualScreen = Screens.Modulo7; 
				
				// Seta que vai reiniciar o modulo 7
				b_isGoingToRestartModulo7 = true;

				break;

				// Caso seja a fase 8
			case 8:

				// Carrega a tela do modulo 8
				enum_actualScreen = Screens.Modulo8; 
				
				// Seta que vai reiniciar o modulo 8
				b_isGoingToRestartModulo8 = true;

				break;

				// Caso seja a fase 9
			case 9:

				// Carrega a tela do modulo 9
				enum_actualScreen = Screens.Modulo9; 
				
				// Seta que vai reiniciar o modulo 9
				b_isGoingToRestartModulo9 = true;

				break;

				// Caso seja a fase 10
			case 10:

				// Carrega a tela do modulo 10
				enum_actualScreen = Screens.Modulo10; 
				
				// Seta que vai reiniciar o modulo 10
				b_isGoingToRestartModulo10 = true;

				break;

			default:

				break;
			}

			// Seta que estah mudando de tela
			b_isChangingScreen = true;

			// Envia para o GameObject MusicController que eh para tocar a musica de INGAME.
			musicController.SetMusic ("Episode");
		}		
	}

	/// <summary>
	/// Telas the fases modulo button.
	/// </summary>
	/// <param name="iCurrentModulo">I current modulo.</param>
	private void TelaFasesModuloButton (int iCurrentModulo)
	{
		// Verifica se os botoes da tela estah ativos.
		if (b_isScreenButtonsActive)
		{
			// Toca o som de botao apertado.
			//audio.PlayOneShot(audio_button, 0.3f);
			audioController.SetPlaySFXAudio (0.3f);
						
			// Guarda a tela anterior
			enum_lastScreen = enum_actualScreen; 
			
			// Verifica o modulo atual
			switch (iCurrentModulo)
			{
				// Caso seja a fase 1
			case 1:

				// Seta que vai reiniciar o modulo 1
				b_isGoingToRestartModulo1 = true;
				
				break;
				
				// Caso seja a fase 2
			case 2:

				// Seta que vai reiniciar o modulo 2
				b_isGoingToRestartModulo2 = true;
				
				break;
				
				// Caso seja a fase 3
			case 3:

				// Seta que vai reiniciar o modulo 3
				b_isGoingToRestartModulo3 = true;
				
				break;
				
				// Caso seja a fase 4
			case 4:
				
				// Seta que vai reiniciar o modulo 4
				b_isGoingToRestartModulo4 = true;
				
				break;
				
				// Caso seja a fase 5
			case 5:

				// Seta que vai reiniciar o modulo 5
				b_isGoingToRestartModulo5 = true;
				
				break;
				
				// Caso seja a fase 6
			case 6:

				// Seta que vai reiniciar o modulo 6
				b_isGoingToRestartModulo6 = true;
				
				break;
				
				// Caso seja a fase 7
			case 7:

				// Seta que vai reiniciar o modulo 7
				b_isGoingToRestartModulo7 = true;
				
				break;
				
				// Caso seja a fase 8
			case 8:

				// Seta que vai reiniciar o modulo 8
				b_isGoingToRestartModulo8 = true;
				
				break;
				
				// Caso seja a fase 9
			case 9:
				
				// Seta que vai reiniciar o modulo 9
				b_isGoingToRestartModulo9 = true;
				
				break;
				
				// Caso seja a fase 10
			case 10:

				// Seta que vai reiniciar o modulo 10
				b_isGoingToRestartModulo10 = true;
				
				break;
				
			default:
				
				break;
			}

			// Seta que estah na tela de fases
			b_isAtLevelScreen = true;

			// Carrega a tela de FASES.
			enum_actualScreen = Screens.TelaFases; 

			// Seta que estah mudando de tela
			b_isChangingScreen = true;

			// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
			musicController.SetMusic ("Episode");
		}		
	}

//	/// <summary>
//	/// Checks the levels completed.
//	/// </summary>
//	/// <returns>The levels completed.</returns>
//	public bool[] CheckLevelsCompleted ()
//	{
//		// Vetor temporario para guardar se as fases foram completadas
//		bool[] array_b_levelsCompleted = new bool[10];
//
//		// Guarda se as fases foram completadas
//		array_b_levelsCompleted[0] = b_isLevel1_1Completed;
//		array_b_levelsCompleted[1] = b_isLevel1_2Completed;
//		array_b_levelsCompleted[2] = b_isLevel1_3Completed;
//		array_b_levelsCompleted[3] = b_isLevel1_4Completed;
//		array_b_levelsCompleted[4] = b_isLevel1_5Completed;
//		array_b_levelsCompleted[5] = b_isLevel1_6Completed;
//		array_b_levelsCompleted[6] = b_isLevel1_7Completed;
//		array_b_levelsCompleted[7] = b_isLevel1_8Completed;
//		array_b_levelsCompleted[8] = b_isLevel1_9Completed;
//		array_b_levelsCompleted[9] = b_isLevel1_10Completed;
//
//		// Retorna as fases que foram completadas
//		return array_b_levelsCompleted;
//	}

	#region case void SetScreen
	/// <summary>
	/// Sets the screen.
	/// </summary>
	public void SetAction (string strActionName)
	{
		// Verifica se os botoes da tela estah ativos.
		if (b_isScreenButtonsActive)
		{
			// Verifica qual eh o Nome da Acao.
			switch (strActionName)
			{
				// Caso seja o botao de LEVEL SELECTION do modulo 1.
			case "TelaFases1":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (1);

				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 2.
			case "TelaFases2":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (2);
				
				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 3.
			case "TelaFases3":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (3);
				
				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 4.
			case "TelaFases4":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (4);
				
				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 5.
			case "TelaFases5":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (5);
				
				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 6.
			case "TelaFases6":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (6);
				
				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 7.
			case "TelaFases7":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (7);
				
				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 8.
			case "TelaFases8":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (8);
				
				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 9.
			case "TelaFases9":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (9);
				
				break;

				// Caso seja o botao de LEVEL SELECTION do modulo 10.
			case "TelaFases10":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de LEVEL SELECTION das telas de MODULO.
				TelaFasesModuloButton (10);
				
				break;

			// Caso seja a tela de Seleçao de Fases.
			case "Home":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Envia para o gameObject MusicController que eh para tocar a musica de SPLASH AND MAIN MENU.
				musicController.SetMusic ("SplashAndMainMenu");

				// CARREGA a scene MainScene.
				SceneController.LoadLevelMainScene();

				break;

				// Caso seja a tela de Seleçao de Fases.
			case "TelaFases":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Envia para o gameObject MusicController que eh para tocar a musica de IN GAME.
				musicController.SetMusic ("Episode");

				// Seta que a camera atual eh de Seleçao de Fases.
				enum_actualScreen = Screens.TelaFases;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;

				// Seta que estah na tela de fases
				b_isAtLevelScreen = true;
				
				break;

				// Caso seja a tela do Modulo1.
			case "Modulo1":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;

				// Seta que a camera atual eh do Modulo1.
				enum_actualScreen = Screens.Modulo1;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");

				break;

				// Caso seja a tela do Modulo2.
			case "Modulo2":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;
				
				// Seta que a camera atual eh do Modulo2.
				enum_actualScreen = Screens.Modulo2;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");
								
				break;

				// Caso seja a tela do Modulo3.
			case "Modulo3":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;

				// Seta que a camera atual eh do Modulo3.
				enum_actualScreen = Screens.Modulo3;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");
				
				break;

				// Caso seja a tela do Modulo4.
			case "Modulo4":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;
				
				// Seta que a camera atual eh do Modulo4.
				enum_actualScreen = Screens.Modulo4;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");
								
				break;

				// Caso seja a tela do Modulo5.
			case "Modulo5":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;
								
				// Seta que a camera atual eh do Modulo5.
				enum_actualScreen = Screens.Modulo5;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");
								
				break;

				// Caso seja a tela do Modulo6.
			case "Modulo6":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;

				// Verifica se as fases do episódio estão destravadas
				if (PlayerPrefs.GetInt("isEpisode" + levelController.GetStrEpisodeName() + "LevelsUnLocked") == 1)
				{
					// Seta que a camera atual eh do Modulo6.
					enum_actualScreen = Screens.Modulo6;

					// Seta que nao estah na tela de fases
					b_isAtLevelScreen = false;
				}

				// As fases estão travadas
				else
				{
					// Seta que a camera atual eh da tela de fases.
					enum_actualScreen = Screens.TelaFases;

					// Seta que é para efetuar a compra das fases
					PlayerPrefs.SetInt("iIsToStartLevelsPurchase", 1);

					// Seta que estah na tela de fases
					b_isAtLevelScreen = true;
				}

				// Seta que estah mudando de tela
				b_isChangingScreen = true;

				// Envia para o gameObject MusicController que eh para tocar a musica do episódio .
				musicController.SetMusic ("Episode");

				break;

				// Caso seja a tela do Modulo7
			case "Modulo7":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;
				
				// Seta que a camera atual eh do Modulo7.
				enum_actualScreen = Screens.Modulo7;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");

				break;

				// Caso seja a tela do Modulo8
			case "Modulo8":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;
				
				// Seta que a camera atual eh do Modulo8.
				enum_actualScreen = Screens.Modulo8;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");

				break;

				// Caso seja a tela do Modulo9
			case "Modulo9":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;
				
				// Seta que a camera atual eh do Modulo9.
				enum_actualScreen = Screens.Modulo9;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");

				break;

				// Caso seja a tela do Modulo10.
			case "Modulo10":

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);

				// Guarda a tela anterior
				enum_lastScreen = Screens.TelaFases;
				
				// Seta que a camera atual eh do Modulo10.
				enum_actualScreen = Screens.Modulo10;

				// Seta que estah mudando de tela
				b_isChangingScreen = true;
				
				// Seta que nao estah na tela de fases
				b_isAtLevelScreen = false;

				// Envia para o gameObject MusicController que eh para tocar a musica de INGAME.
				musicController.SetMusic ("Episode");

				break;

				// Caso seja o botao de restart do modulo 1.
			case "Restart1":

				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (1);

				break;

				// Caso seja o botao de restart do modulo 2.
			case "Restart2":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (2);
				
				break;

				// Caso seja o botao de restart do modulo 3.
			case "Restart3":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (3);
				
				break;
				// Caso seja o botao de restart do modulo 4.
			case "Restart4":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (4);
				
				break;

				// Caso seja o botao de restart do modulo 5.
			case "Restart5":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (5);
				
				break;

				// Caso seja o botao de restart do modulo 6.
			case "Restart6":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (6);
				
				break;

				// Caso seja o botao de restart do modulo 7.
			case "Restart7":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (7);
				
				break;

				// Caso seja o botao de restart do modulo 8.
			case "Restart8":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (8);
				
				break;

				// Caso seja o botao de restart do modulo 9.
			case "Restart9":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (9);
				
				break;

				// Caso seja o botao de restart do modulo 10.
			case "Restart10":
				
				// Chama funcao dentro do ScreenController que lida com os botoes de restart das telas de Feedback.
				FeedBackRestartButton (10);
				
				break;

				// Caso seja o botão de NEXT para a tela da Galeria de Selos.
			case "StampsGallery":

				// Reinicia as fases
				levelController.RestartLevels();

				// Toca o som de botao apertado.
				audioController.SetPlaySFXAudio (0.3f);
				
				// Envia para o gameObject MusicController que eh para tocar a musica de SPLASH AND MAIN MENU.
				musicController.SetMusic ("SplashAndMainMenu");

				// Seta que deve mostrar a galeria de selos
				PlayerPrefs.SetInt("iIsToShowStampGallery", 1);

				// CARREGA a scene MainScene.
				SceneController.LoadLevelMainScene();

				break;

			default:
				
				break;
			}
		}
	}
	#endregion
}