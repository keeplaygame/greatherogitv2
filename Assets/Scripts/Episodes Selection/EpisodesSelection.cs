﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//
// LISTA DE EPISÓDIOS
// 1 - Davi
//

public class EpisodesSelection : MonoBehaviour 
{	
	[SerializeField]
	// Recebe e guarda o sfx controller
	private SFXController sfxController;

	[SerializeField]
	private GameObject go_loadingBackgroundImage;
	
	// Contador da animaçao do scroll rect
	private int i_scrollRectAnimationCounter;

	void Start ()
	{
		// Seta que o contador inicia em 0
		i_scrollRectAnimationCounter = 0;
	}

	/// <summary>
	/// Sets the episode to go.
	/// </summary>
	/// <param name="strEpisodeToGo">String episode to go.</param>
	public void SetEpisodeToGo (string strEpisodeToGo)
	{
//		// Inicia a corotina para carregar a proxima cena
//		StartCoroutine("ShowActivity", strEpisodeToGo);

		// Verifica qual episodio deve ir
		switch(strEpisodeToGo)
		{
		case ("EpisodeAdamAndEve"):

			// Carrega a cena do episodio de Adão e Eva.
			SceneController.LoadLevelEpisodeAdamAndEve();
						
			break;
			
		case ("EpisodeDavidAndGoliath"):

			// Carrega a cena do episodio do Davi e Golias.
			SceneController.LoadLevelEpisodeDavidAndGoliath();
			
			break;
			
		case ("EpisodeNoahsArk"):
			
			// Carrega a cena do episodio do Davi e Golias.
			SceneController.LoadLevelEpisodeNoahsArk();
			
			break;
			
		case ("Episode_4"):
			
						
			break;
			
		case ("Episode_5"):
			
						
			break;
			
		case ("Episode_6"):
			
					
			break;
			
		case ("Episode_7"):
			
						
			break;
			
		case ("Episode_8"):
			
						
			break;
			
		case ("Episode_9"):
			
						
			break;
			
		case ("Episode_10"):
			
						
			break;
			
		default:
			
			break;
		}
	}

//	/// <summary>
//	/// Moves the scroll rect left.
//	/// </summary>
//	public void MoveScrollRectLeft ()
//	{
//		// Seta que o contador inicia em 0
//		i_scrollRectAnimationCounter = 0;
//
//		// Inicia a animacao de mover para a esquerda
//		StartCoroutine("AnimationMoveScroolRectLeft");
//
//		// Toca o som do botão
//		sfxController.SetPlayAudioButton(1.0f);
//	}
//
//	/// <summary>
//	/// Moves the scroll rect right.
//	/// </summary>
//	public void MoveScrollRectRight ()
//	{
//		// Seta que o contador inicia em 0
//		i_scrollRectAnimationCounter = 0;
//
//		// Inicia a animacao de mover para a direita
//		StartCoroutine("AnimationMoveScroolRectRight");
//
//		// Toca o som do botão
//		sfxController.SetPlayAudioButton(1.0f);
//	}
//
//	/// <summary>
//	/// Animations the move scrool rect right.
//	/// </summary>
//	/// <returns>The move scrool rect right.</returns>
//	IEnumerator AnimationMoveScroolRectRight ()
//	{
//		// Movimenta o scroll para a direita
//		scrollRect_categoryScrollSelectionPanel.horizontalNormalizedPosition += 0.0131f;
//
//		// Aguarda um tempo para movimentar mais
//		yield return new WaitForSeconds (0.01f);
//
//		// Incrementa o contador da animaçao
//		i_scrollRectAnimationCounter++;
//
//		// Verifica se ainda nao movimentou 10 vezes
//		if (i_scrollRectAnimationCounter < 10)
//		{
//			// Inicia a animacao de mover para a direita
//			StartCoroutine("AnimationMoveScroolRectRight");
//		}
//	}
//
//	/// <summary>
//	/// Animations the move scrool rect left.
//	/// </summary>
//	/// <returns>The move scrool rect left.</returns>
//	IEnumerator AnimationMoveScroolRectLeft ()
//	{
//		// Movimenta o scroll para a direita
//		scrollRect_categoryScrollSelectionPanel.horizontalNormalizedPosition -= 0.0131f;
//		
//		// Aguarda um tempo para movimentar mais
//		yield return new WaitForSeconds (0.01f);
//		
//		// Incrementa o contador da animaçao
//		i_scrollRectAnimationCounter++;
//		
//		// Verifica se ainda nao movimentou 10 vezes
//		if (i_scrollRectAnimationCounter < 10)
//		{
//			// Inicia a animacao de mover para a direita
//			StartCoroutine("AnimationMoveScroolRectLeft");
//		}
//	}
}