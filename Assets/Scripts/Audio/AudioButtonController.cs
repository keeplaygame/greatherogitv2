﻿using UnityEngine;
using System.Collections;

public class AudioButtonController : MonoBehaviour 
{
	// Recebe e guarda o GameObject que tem o AudioOnButton.
	public GameObject go_audioOnButton;

	// Recebe e guarda o GameObject que tem o AudioOffButton.
	public GameObject go_audioOffButton;

	void Update ()
	{
		// Verifica se o Audio Status é ON
		if(AudioListener.volume == 1)
		{
			// Seta que o audio está ligado
			SetAudioButtonStatus("AudioOn");
		}
		
		// Verifica se o Audio Status é OFF
		else
		{
			// Seta que o audio está desligado
			SetAudioButtonStatus("AudioOff");
		}
	}

	/// <summary>
	/// Sets the audio button status.
	/// </summary>
	/// <param name="strAudioStatus">String audio status.</param>
	public void SetAudioButtonStatus(string strAudioStatus)
	{
		// Switch do status do Audio.
		switch(strAudioStatus)
		{
		
		// CASO seja o status ON.
		case("AudioOn"):

			// ATIVA o GameObject do Audio Button ON.
			go_audioOnButton.SetActive(true);

			// DESATIVA o GameObject do Audio Button OFF.
			go_audioOffButton.SetActive(false);

			break;

		// CASO seja o status ON.
		case("AudioOff"):
			
			// DESATIVA o GameObject do Audio Button ON.
			go_audioOnButton.SetActive(false);
			
			// ATIVA o GameObject do Audio Button OFF.
			go_audioOffButton.SetActive(true);
			
			break;

		default:

			break;
		}
	}
}