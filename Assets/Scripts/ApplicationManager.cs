﻿using UnityEngine;
using System.Collections;

public class ApplicationManager : MonoBehaviour {

	// DEFINE array para guardar os gameObjects.
	public GameObject[] goArray_developmentBuilds;

	void Awake () {

		#if DEVELOPMENT_BUILD
			// 0 - Versão player.
			// 1 - Versão de desenvolvimento.
			// Seta que a build é de development.
			PlayerPrefs.SetInt("BisDevelopmentBuild", 1);
		#else 
			// CASO seja qualquer outra plataforma.

			// 0 - Versão player.
			// 1 - Versão de desenvolvimento.
			// DELETA a key que indica se a build é de development.
			PlayerPrefs.DeleteKey("BisDevelopmentBuild");
		#endif

		// VERIFICA se tem a PlayerPrefs Key de "BisDevelopmentBuild"
		if(!PlayerPrefs.HasKey("BisDevelopmentBuild")) {
			// GUARDA todos os gameObjects com a tag "DevelopmentBuild".
			goArray_developmentBuilds = GameObject.FindGameObjectsWithTag("DevelopmentBuild");
			
			// Para cada gameObject com tag "DevelopmentBuild", desativa o gameObject.
			foreach (GameObject go_developmentBuild in goArray_developmentBuilds) {
				// DESATIVA o gameObject.
				go_developmentBuild.SetActive(false);
			}
		}
	}

	void Update(){
		#if UNITY_ANDROID
		// VERIFICA se o botão de back do device android foi apertado.
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}			
		#endif
	}

	// Função foi pega do package da asset store https://www.assetstore.unity3d.com/en/#!/content/25468
	public void Quit () {
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
}