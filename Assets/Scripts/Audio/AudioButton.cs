﻿using UnityEngine;
using System.Collections;

public class AudioButton : MonoBehaviour 
{
	// Recebe e guarda o GameObject que tem o AudioController Script.
	public AudioController audioController;

	// Recebe o audio button controller
	public AudioButtonController audioButtonController;

	// Guarda o nome da acao.
	public string str_actionName;

	// Indica se o botao estah ativo
	private bool b_isActive;

	// Guarda a textura normal
	public Texture2D texture_normal;

	// Guarda a textura de hover
	public Texture2D texture_hover;

	void Start ()
	{
		// Seta que o bota nao comeca ativo
		b_isActive = false;

		// SETA a textura inicial do botão como sendo a textura normal.
		guiTexture.texture = texture_normal;
	}

	void OnMouseExit()
	{
		// Volta a textura normal do botao
		guiTexture.texture = texture_normal;

		// Seta que o botao nao estah ativo
		b_isActive = false;
	}

	void OnMouseDown()
	{
		// Seta a textura hover do botao
		guiTexture.texture = texture_hover;

		// Seta que o botao estah ativo
		b_isActive = true;
	}

	void OnMouseUp()
	{
		// Verifica se o botao estah ativo
		if (b_isActive)
		{
			// Volta a textura normal do botao
			guiTexture.texture = texture_normal;
			
			// Envia para o AUDIOCONTROLLER a acao.
			audioController.SetAction(str_actionName);

			// Envia para o audio button controller a acao
			audioButtonController.SetAudioButtonStatus(str_actionName);
		}
	}
}