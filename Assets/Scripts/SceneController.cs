﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour 
{
	/// <summary>
	/// Loads the level main scene without show activity.
	/// </summary>
	public static void LoadLevelMainSceneWithoutShowActivity ()
	{
		// Carrega a main scene
		Application.LoadLevel("MainScene");

		// TIRA da memória assets que não estão sendo utilizados.
		Resources.UnloadUnusedAssets ();
	}

	/// <summary>
	/// Loads the level main scene.
	/// </summary>
	public static void LoadLevelMainScene ()
	{
		// Mostra que está tendo atividade
		LoadingController.ShowActivity();

		// Carrega a main scene
		Application.LoadLevel("MainScene");

		// TIRA da memória assets que não estão sendo utilizados.
		Resources.UnloadUnusedAssets ();
	}

	/// <summary>
	/// Loads the level episode adam and eve.
	/// </summary>
	public static void LoadLevelEpisodeAdamAndEve ()
	{
		// Mostra que está tendo atividade
		LoadingController.ShowActivity();
		
		// Carrega a Scene EpisodeAdamAndEve.
		Application.LoadLevel("EpisodeAdamAndEve");
		
		// TIRA da memória assets que não estão sendo utilizados.
		Resources.UnloadUnusedAssets ();
	}

	/// <summary>
	/// Loads the level episode david and goliath.
	/// </summary>
	public static void LoadLevelEpisodeDavidAndGoliath ()
	{
		// Mostra que está tendo atividade
		LoadingController.ShowActivity();
		
		// Carrega a Scene EpisodeDavidAndGoliath
		Application.LoadLevel("EpisodeDavidAndGoliath");

		// TIRA da memória assets que não estão sendo utilizados.
		Resources.UnloadUnusedAssets ();
	}

	/// <summary>
	/// Loads the level episode noahs ark.
	/// </summary>
	public static void LoadLevelEpisodeNoahsArk ()
	{
		// Mostra que está tendo atividade
		LoadingController.ShowActivity();
		
		// Carrega a Scene EpisodeNoahsArk.
		Application.LoadLevel("EpisodeNoahsArk");
		
		// TIRA da memória assets que não estão sendo utilizados.
		Resources.UnloadUnusedAssets ();
	}
}
