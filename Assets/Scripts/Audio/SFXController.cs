﻿using UnityEngine;
using System.Collections;

public class SFXController : MonoBehaviour 
{
	[SerializeField]
	// Recebe e guarda áudio que toca ao apertar algum botão.
	private AudioClip audio_button; 

	[SerializeField]
	// Recebe e guarda o áudio que toca quando ganha um novo selo
	private AudioClip audio_newStamp;

	/// <summary>
	/// Sets the play audio button.
	/// </summary>
	/// <param name="fVolume">F volume.</param>
	public void SetPlayAudioButton (float fVolume)
	{
		// Toca o som de botão apertado.
		audio.PlayOneShot(audio_button, fVolume);
	}

	public void SetPlayAudioNewStamp (float fVolume)
	{
		// Toca o som de novo selo
		audio.PlayOneShot(audio_newStamp, fVolume);
	}
}